# Requirements

- `make`, `rm`, `cp`, `mkdir`, and `xdg-open`
- `latexmk` for LaTeX compilation
- `aspell` for spell-checking

# Usage

## Basic Usage

- Download the [latest release](https://gitlab.com/jerg/latex-make/-/releases/permalink/latest)
- Create a git repo in the folder
- `make` to output main.pdf in `/out`
- Change LaTeX compilers and other settings in ``Makefile``

## Images

- Images are stored in `/img`

## Multiple Files

- Chapters in `main.tex` and chapters should be stored in `/tex`

## Commands

- `make all` to output pdf in `/out`
- `make live` uses latexmk constantly compile
- `make clean` removes all compiles files
