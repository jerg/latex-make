# Define the latex compiler
LATEX = pdflatex

# Define the file name, without .tex
FILE = main

# Define the output name
OUTPUT_NAME = main_out

# Define the output and build directory
BUILD_DIR = ./.tmp
OUT_DIR = ./out

all: 
	mkdir -p $(BUILD_DIR)
	mkdir -p $(OUT_DIR)
	rm -rf '$(BUILD_DIR)'/*
	rm -rf '$(OUT_DIR)'/*
	chktex -q '$(FILE).tex'
	aspell --mode=tex check '$(FILE).tex'
	latexmk -$(LATEX) --output-directory=$(OUT_DIR) -aux-directory=$(BUILD_DIR) -jobname=$(OUTPUT_NAME) -halt-on-error -interaction=nonstopmode '$(FILE).tex'

live:
	mkdir -p $(BUILD_DIR)
	mkdir -p $(OUT_DIR)
	latexmk -pvc -$(LATEX) --output-directory=$(OUT_DIR) -aux-directory=$(BUILD_DIR) -jobname=$(OUTPUT_NAME) -halt-on-error -interaction=nonstopmodee '$(FILE).tex'

open:
	xdg-open '$(OUT_DIR)/$(OUTPUT_NAME).pdf'

clean: 
	rm -rf '$(BUILD_DIR)'/*
	rm -rf '$(OUT_DIR)'/*
